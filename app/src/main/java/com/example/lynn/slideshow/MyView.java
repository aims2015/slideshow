package com.example.lynn.slideshow;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import static com.example.lynn.slideshow.MainActivity.*;

/**
 * Created by lynn on 6/24/2015.
 */

public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        int[] ids = new int[]{R.drawable.alf1,
                R.drawable.alf2,
                R.drawable.alf3,
                R.drawable.alf4,
                R.drawable.alf5,
                R.drawable.alf6,
                R.drawable.alf7,
                R.drawable.alf8,
                R.drawable.alf9,
                R.drawable.alf10,
                R.drawable.alf11,
                R.drawable.alf12,
                R.drawable.alf13,
                R.drawable.alf14,
                R.drawable.alf15,
                R.drawable.alf16,
                R.drawable.alf17,
                R.drawable.alf18,
                R.drawable.alf19,
                R.drawable.alf20,
                R.drawable.alf21,
                R.drawable.alf23,
                R.drawable.alf24,
                R.drawable.alf25,
                R.drawable.alf26,
                R.drawable.alf27,
                R.drawable.alf28,
                R.drawable.alf29,
                R.drawable.alf30,
                R.drawable.alf31,
                R.drawable.alf32,
                R.drawable.alf33,
                R.drawable.alf34,
                R.drawable.alf35,
                R.drawable.alf36,
                R.drawable.alf37,
                R.drawable.brian,
                R.drawable.kate,
                R.drawable.lynn,
                R.drawable.wille};

        drawables = new Drawable[ids.length];

        for (int counter=0;counter<drawables.length;counter++)
            drawables[counter] = getResources().getDrawable(ids[counter]);

        view = new ImageView(context);

        view.setScaleType(ImageView.ScaleType.FIT_CENTER);

        index = 0;

        view.setImageDrawable(drawables[index]);

        view.setOnTouchListener(listener);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(600,5600);

        layoutParams.leftMargin = 200;
        layoutParams.topMargin = 100;

        view.setLayoutParams(layoutParams);

        addView(view);
    }

}
